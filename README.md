# Supported tags and respective ```Dockerfile``` links

* [```latest``` (*Dockerfile*)][1]

# What is BTSync?
[BitTorrent Sync](https://www.getsync.com/individuals/) is a proprietary peer-to-peer file synchronization tool available for Windows, Mac, Linux, Android, iOS, Windows Phone, Amazon Kindle Fire and BSD. It can sync files between devices on a local network, or between remote devices over the Internet via a modified version of the BitTorrent protocol.

# How do use this image
```docker run -d -p 8888:8888 -p 55555:55555 -v /BtConfigDir:/btsync skeletorsue/btsync```

[1]: https://bitbucket.org/skeletorsue/docker-btsync/src/master/Dockerfile?fileviewer=file-view-default
